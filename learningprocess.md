# What is Feynman Technique?
The Feynman Technique is a method by which we learn by explaining the concept in simple terms to others.
# Steps 
* Select a concept to learn.
* Teach it to a child.
* Review your understanding.
* Organize your notes and revisit them regularly.

# what was the most interesting story or idea for you?
* The most interesting story or idea was how she struggled with math and science early on but later became an engineering professor, she conveys that anyone can learn difficult subjects with the right approach.

# What are active and diffused modes of thinking?
* Active mode of thinking is focused, conscious and logical used for problem-solving and learning new skills.
* Diffused mode of thinking is relaxed, subconscious way of thinking that allows for creativity.

# According to the video, what are the steps to take when approaching a new topic? Only mention the points.
* Break the skill into smaller parts.
* Learn a little to understand the basics.
* Get rid of things that distract us.
* Practice for as much as hours possible.

# What are some of the actions you can take going forward to improve your learning process?
* Focus on work by avoiding things that distracting us.
* Understand the things by breaking them down in to small topics.
* Stay curious to learn new things.
* Teach concepts to friends helpful for self correction.
* Keep organized notes for quick and easy revision.







