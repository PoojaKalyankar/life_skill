# What are the steps/strategies to do Active Listening? 
* Focusing on the Speaker.
* Try not to interrupt the other person.
* By using door openers like tell me more.
* Utilize the body language to show your listening.
* Note down the important conversation.
* Manage your own thoughts.

# According to Fisher's model, what are the key points of Reflective Listening? 
* Reflective listening means understanding the speaker's idea and then repeating it back to them to make sure you got it right. It's useful in important talks, meetings, and emotional situations. In business, take notes, share them with everyone involved, and confirm everyone agrees on what was discussed.

# What are the obstacles in your listening process?
* Obstacles in my listening process include distractions like noise, personal biases, mostly repeated things and not focusing. Interrupting the speaker while conveying important points make me distracts. Being tired or hungry can also affect my concentration.

# What can you do to improve your listening?
* Improving listening involves giving full attention, not interrupting, asking questions, understanding their point of view, taking notes, managing our own thoughts and finding interest in the conversation.

# When do you switch to Passive communication style in your day to day life?
* When I don't want to hurt other person.
* When I feel nervous.
* When I don't want to make things complicate.

# When do you switch into Aggressive communication styles in your day to day life?
* When I feel insult.
* When someone blame me for their mistakes.

# When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
* When I agree to do something but intentionally, I don't want.
* When people tells lie to me.
* When I don't want to hurt others.

# How can you make your communication assertive? 
* By Speaking Confidently.
* Maintain eye contact and body language.
* Avoid overthinking and trust the othe person.
* Clearly identify what we need and express directly.
* Speak up about what bothering us.